#pragma once
#include "functions.h"
string server, filepath, filename;
string protocol = "";
string url = "";
HINSTANCE hInst;
WSADATA wsaData;
string downloadFolderPath = "1412082_lectures";
float httpVersion = 1.0;
bool isDownloading = false;
int main(int argc, char *argv[])
{

#ifdef DEBUG_FOLDER
	char * command[] = { "", "--http1.1", "http://students.iitk.ac.in/programmingclub/course/lectures/" };
	if (!isArgumentValid(3, command)) {
		system("Pause");
		return false;
}
#else
#ifdef DEBUG_SINGLE_FILE_MP3
	char * command[] = { "", "--http1.0", "http://www.cedarhome.org/mp3/songs/01%20Battlecry.mp3" };
	if (!isArgumentValid(3, command)) {
		system("Pause");
		return false;
	}
#else
#ifdef DEBUG_SINGLE_FILE_READABLE
	char * command[] = { "", "--http1.1", "http://students.iitk.ac.in/programmingclub/course/lectures/1.%20Introduction%20to%20C%20language%20and%20Linux.pdf" };
	if (!isArgumentValid(3, command)) {
		system("Pause");
		return false;
	}

#else
	if (!isArgumentValid(argc, argv)) {
		system("Pause");
		return false;
	}
#endif //DEBUG_SINGLE_FILE_READABLE
#endif //DEBUG_SINGLE_FILE_MP3
#endif //DEBUG
	if (WSAStartup(0x101, &wsaData) != 0)
		return -1;
	char * localUrl  = new char[url.length() + 1];
	std::strcpy(localUrl, url.c_str());
	thread downloadThread (downloadFromURL);
	downloadThread.join();
	system("pause");
	WSACleanup();
	return 0;
}


void downloadFromURL()
{
	char *localUrl = new char[url.length() + 1];
	strcpy(localUrl, url.c_str());
	const int bufSize = 1024;
	bool isSingleFile = false;
	char readBuffer[bufSize], sendBuffer[bufSize], tmpBuffer[bufSize];
	char *tmpResult = NULL, *result;
	SOCKET conn;
	long totalBytesRead, thisReadSize, headerLen;
	mParseUrl(localUrl, server, filepath, filename);

	///////////// step 1, connect //////////////////////
	conn = connectToServer((char*)server.c_str(), 80);

	///////////// step 2, send GET request /////////////
	sprintf(tmpBuffer, "GET %s HTTP/%.1f", filepath.c_str(), httpVersion);
	strcpy(sendBuffer, tmpBuffer);
	strcat(sendBuffer, "\r\n");
	sprintf(tmpBuffer, "Host: %s", server.c_str());
	strcat(sendBuffer, tmpBuffer);
	strcat(sendBuffer, "\r\n");
	strcat(sendBuffer, "\r\n");
	send(conn, sendBuffer, strlen(sendBuffer), 0);

	//    SetWindowText(edit3Hwnd, sendBuffer);
	printf("Buffer being sent:\n%s", sendBuffer);

	///////////// step 3 - get received bytes ////////////////
	// Receive until the peer closes the connection
	totalBytesRead = 0;
	
	isDownloading = true;
	thread downloadProgreseUI(printDownloadProgressingUI);
	downloadProgreseUI.detach();
	while (1)
	{
		memset(readBuffer, 0, bufSize);
		thisReadSize = recv(conn, readBuffer, bufSize, 0);

		if (thisReadSize <= 0)
		{
			isDownloading = false;
			break;
		}
		tmpResult = (char*)realloc(tmpResult, thisReadSize + totalBytesRead);

		memcpy(tmpResult + totalBytesRead, readBuffer, thisReadSize);
		totalBytesRead += thisReadSize;
	}
	if (filename.find(".") != string::npos) {
		result = new char[totalBytesRead + 1];
		memcpy(result, tmpResult, totalBytesRead);
		result[totalBytesRead] = 0x0;
		saveDownloadFile(totalBytesRead, result);
		delete(tmpResult);
		
	}
	else {
		headerLen = getHeaderLength(tmpResult);
		long contenLen = totalBytesRead - headerLen;
		result = new char[contenLen + 1];
		memcpy(result, tmpResult + headerLen, contenLen);
		result[contenLen] = 0x0;
		char *myTmp;

		myTmp = new char[headerLen + 1];
		strncpy(myTmp, tmpResult, headerLen);
		myTmp[headerLen] = NULL;
		delete(tmpResult);

		saveDownloadFiles(contenLen, result);
	}
	closesocket(conn);
	delete[] localUrl;
	terminate;
}

std::string urlDecode(std::string &eString) {
	std::string ret;
	char ch;
	int i, j;
	for (i = 0; i<eString.length(); i++) {
		if (int(eString[i]) == 37) {
			sscanf(eString.substr(i + 1, 2).c_str(), "%x", &j);
			ch = static_cast<char>(j);
			ret += ch;
			i = i + 2;
		}
		else {
			ret += eString[i];
		}
	}
	return (ret);
}

bool getProtocol(string command) {
	if (command == "--http1.0" || command == "--http1.1") {
		protocol = command;
		if (command == "--http1.1") {
			httpVersion = 1.1;
		}
		return true;
	}
	return false;
}

bool getUrl(string command) {
	url = command;
	return true;
}

bool isArgumentValid(int argc, char *argv[]) {
	if (argc != 3) {
		cout << "Command is invalid!!!" << endl;
		return false;
	}
	if (getProtocol(argv[1])) {
		getUrl(argv[2]);
	}
	else {
		getProtocol(argv[2]);
		getUrl(argv[1]);
	}
	if (protocol == "") {
		cout << "Protocol is invalid!!!" << endl;
		return false;
	}
	if (url == "") {
		cout << "Protocol is invalid!!!" << endl;
		return false;
	}
	cout << "Protocol: " << protocol << endl;
	cout << "Url: " << url << endl;
	return true;
}
void printDownloadProgressingUI() {
	cout << "Download file(s) ...";
	while (isDownloading) {
		cout << '.';
		Sleep(100);
	}
	cout << " completed!" << endl;
	terminate;
}

void saveDownloadFiles(long fileSize, char * buffer) {
	FILE *fp;
	if (fileSize != 0)
	{
		mkdir(downloadFolderPath.c_str());
		string bufferString = buffer;
		set<string> fileUrls = extract_hyperlinks(bufferString);
		for (auto fName : fileUrls) {
			if (fName.back() == '/') {
				if (fName == filepath) {
					continue;
				}
				else {
					mkdir(urlDecode(fName).c_str());
				}
				continue;
			}
			else if (fName.find('.') == string::npos) { //not contain '.' in name
				continue;
			}
			string fileUrl = server + filepath + fName;
			string decodedFileName = urlDecode(fName);
			string filePath = downloadFolderPath + "/" + decodedFileName;
			fp = fopen(filePath.c_str(), "wb");
			fwrite(buffer, 1, fileSize, fp);
			fclose(fp);
		}
	}
}

void saveDownloadFile(long fileSize, char * buffer) {
	string localFileName = "1412082_" + urlDecode(filename);
	FILE *fp = fopen(localFileName.c_str(), "wb");
	fwrite(buffer, 1, fileSize, fp);
	fclose(fp);
}