#ifndef __SOCKET_FUNCTIONS_H__
#define __SOCKET_FUNCTIONS_H__

#include <windows.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <regex>
#include <vector>
#include <set>
#include <fstream>
#include <ctype.h>
#include <direct.h>
#include <thread>

using namespace std;
#pragma comment(lib,"ws2_32.lib")
//#define DEBUG_FOLDER

#define DEBUG_SINGLE_FILE_MP3

#define DEBUG_SINGLE_FILE_READABLE

void mParseUrl(char *mUrl, string &serverName, string &filepath, string &filename);
SOCKET connectToServer(char *szServerName, WORD portNum);
int getHeaderLength(char *content);
void downloadFromURL();
string readFile(const char * fileName);
set<string> extract_hyperlinks(std::string text);
std::string urlDecode(std::string &eString);

bool getProtocol(string command);
bool getUrl(string command);

bool isArgumentValid(int argc, char *argv[]);

void printDownloadProgressingUI();

void saveDownloadFiles(long fileSize, char * buffer);
void saveDownloadFile(long fileSize, char * buffer);
#endif // !__SOCKET_FUNCTIONS_H__